# README #

Makes last item in your sidebar to be sticky and not disappear as you scroll down.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

Include the javascript in your html file and make a div sticky using -  
$(document).ready(function() {  
  $.stickysidebarscroll("#<id_of_last_element>",{offset: {top: 10, bottom: 200}});  
});  

